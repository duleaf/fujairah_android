package com.Duleaf.Fujairah.Activities;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.Duleaf.Fujairah.Activities.R.id;
import com.Duleaf.Fujairah.Adapters.CallUsListAdapter;
import com.Duleaf.Fujairah.Adapters.LangaugeController;
import com.Duleaf.Fujairah.Model.FujConstants;

public class CallUsFragment extends Fragment {

	ListView list;
	CallUsListAdapter adapter; 
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.call_us, container, false);
         
        list = (ListView) rootView.findViewById(R.id.listView1);
        
        
        
        ArrayList<String> info = new ArrayList<String>();
        
   
        
        if(LangaugeController.getCurrentLangauge().equalsIgnoreCase(FujConstants.AR_Lang))
        {
        	 info.add("هاتف");
             info.add("123451231");
             info.add("بريد الكتروني");
             info.add("email@email.com");
             info.add("فاكس");
             info.add("123451231");
             info.add("فيسبوك");
             info.add("fb/fujairah");
             info.add("تويتر");
             info.add("@Fujairah");
             info.add("الموقع الالكتروني");
             info.add("www.fujairah.ae");
        }
        else
        {
            info.add("Phone");
            info.add("123451231");
            info.add("Email");
            info.add("email@email.com");
            info.add("Fax");
            info.add("123451231");
            info.add("Facebook");
            info.add("fb/fujairah");
            info.add("Twitter");
            info.add("@Fujairah");
            info.add("Website");
            info.add("www.fujairah.ae");
        }
        
        
        adapter = new CallUsListAdapter(getActivity(), info);
        list.setAdapter(adapter);
        
        return rootView;
        
	}
}
