
package com.Duleaf.Fujairah.Activities;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.content.Entity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.Duleaf.Fujairah.Adapters.LangaugeController;
import com.Duleaf.Fujairah.Model.FujConstants;
import com.Duleaf.Fujairah.Model.GovernmentEntity;
import com.squareup.picasso.Picasso;

@SuppressLint("NewApi")
public class GovernmentEntityDetails extends Fragment {


	GovernmentEntity govEntity;
	
	ImageView backGroundImage;
	ImageView logo;
	
	Button phone;
	Button fb;
	Button location;
	Button tw;
	Button web;
	
	
	TextView intro;
	TextView targets;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.gov_entity_details, container, false);
         
        intro = (TextView) rootView.findViewById(R.id.intro_detials);
        targets = (TextView) rootView.findViewById(R.id.targets_details);
        
        backGroundImage = (ImageView) rootView.findViewById(R.id.gov_image);
        logo = (ImageView) rootView.findViewById(R.id.gov_logo);
        
        if(LangaugeController.getCurrentLangauge().equalsIgnoreCase(FujConstants.AR_Lang))
        {
        	intro.setLayoutDirection(LayoutDirection.RTL);
        	targets.setLayoutDirection(LayoutDirection.RTL);
        }

        Log.i("image",govEntity.getLogo_uri());
        Picasso.with(getActivity()).load(govEntity.getImage()).into(backGroundImage);
        Picasso.with(getActivity()).load(govEntity.getLogo_uri()).into(logo);
        
        intro.setText(govEntity.getIntro());
        targets.setText(govEntity.getDesc());
        
        
        phone = (Button) rootView.findViewById(R.id.phone);
        fb = (Button) rootView.findViewById(R.id.fb);
        location = (Button) rootView.findViewById(R.id.location);
        tw = (Button) rootView.findViewById(R.id.tw);
        web = (Button) rootView.findViewById(R.id.web);
        
        
        phone.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				String uri = "tel:" + govEntity.getPhone() ;
				 Intent intent = new Intent(Intent.ACTION_CALL);
				 intent.setData(Uri.parse(uri));
				 getActivity().startActivity(intent);
				
			}
		});
        
        
        web.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(govEntity.getWebSite_url()));
				startActivity(browserIntent);
			}
		});
        
        fb.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent browserIntent;// = new Intent(Intent.ACTION_VIEW, Uri.parse(govEntity.getFB()));
				
				String pageName = govEntity.getFB();
//				pageName = pageName.lastIndexOf("/");
				try {
				    getActivity().getPackageManager().getPackageInfo("com.facebook.katana", 0);
				    browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/<id_here>"));
				   } catch (Exception e) {
					   browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/<user_name_here>"));
				   }
				startActivity(browserIntent);
			}
		});
        
        
        location.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
			}
		});
        
        
        tw.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
					
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(govEntity.getTW()));
				startActivity(browserIntent);
				
			}
		});
        
        
        
        return rootView;
    }
}