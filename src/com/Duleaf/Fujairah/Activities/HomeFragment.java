package com.Duleaf.Fujairah.Activities;

import java.util.Stack;

import com.Duleaf.Fujairah.Adapters.BackButton;

import android.annotation.SuppressLint;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

@SuppressLint("NewApi")
public class HomeFragment extends Fragment implements BackButton {

	ImageView image1 ;
	ImageView image2 ;
	ImageView image3 ;
	
	RelativeLayout details1;
	RelativeLayout details2;
	RelativeLayout details3;
	
	RelativeLayout cell1;
	RelativeLayout cell2;
	RelativeLayout cell3;
	
	private Stack<Fragment> fragmentStack;

	
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.home, container, false);
        
        fragmentStack = new Stack<Fragment>();
        fragmentStack.push(this);
        
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        
        cell1 = (RelativeLayout) rootView.findViewById(R.id.cell1);
        cell2 = (RelativeLayout) rootView.findViewById(R.id.cell2);
        cell3 = (RelativeLayout) rootView.findViewById(R.id.cell3);

        
        cell1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				Log.i("Cell 1 ","Clicked");
				
				Fragment newFragment = new FujRulerFragment();
				FragmentTransaction transaction = getFragmentManager().beginTransaction();

				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.add(R.id.frame_container, newFragment);
				transaction.hide(fragmentStack.lastElement());
				transaction.addToBackStack(null);

				// Commit the transaction
				transaction.commit();
				fragmentStack.push(newFragment);
			}
		});
        
        
        cell2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				Log.i("Cell 2 ","Clicked");
				
				Fragment newFragment = new AboutEmaratFragment();
				FragmentTransaction transaction = getFragmentManager().beginTransaction();

				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.add(R.id.frame_container, newFragment);
				transaction.hide(fragmentStack.lastElement());
				transaction.addToBackStack(null);

				// Commit the transaction
				transaction.commit();
				fragmentStack.push(newFragment);
			}
		});
        
        
        cell3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Log.i("Cell 3 ","Clicked");
				
				Fragment newFragment = new HistoryOfFujFragment();
				FragmentTransaction transaction = getFragmentManager().beginTransaction();

				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.add(R.id.frame_container, newFragment);
				transaction.hide(fragmentStack.lastElement());
				transaction.addToBackStack(null);

				// Commit the transaction
				transaction.commit();
				fragmentStack.push(newFragment);
				
			}
		});
        
        cell3.setLayoutDirection(LayoutDirection.RTL);
        
        details1 = (RelativeLayout) rootView.findViewById(R.id.details1);
        details2 = (RelativeLayout) rootView.findViewById(R.id.details2);
        details3 = (RelativeLayout) rootView.findViewById(R.id.details3);
        
        image1 = (ImageView) rootView.findViewById(R.id.img1);
        image2 = (ImageView) rootView.findViewById(R.id.img2);
        image3 = (ImageView) rootView.findViewById(R.id.img3);
        
        details1.setVisibility(View.INVISIBLE);
        details3.setVisibility(View.INVISIBLE);
        details2.setVisibility(View.INVISIBLE);
        
        
        TranslateAnimation imgAnimation = new TranslateAnimation(width - image1.getWidth(), 0, 0, 0);
        imgAnimation.setDuration(500);
        image1.startAnimation(imgAnimation);
        image3.startAnimation(imgAnimation);
        
        Log.i("Width", ""+width);
        Log.i("IMGWidth", ""+image1.getWidth());
        Log.i("DETWidth", ""+details1.getWidth());
        
        
        TranslateAnimation animation = new TranslateAnimation(-(width /2) ,0, 0, 0);
        animation.setDuration(500);        
        image2.startAnimation(animation);
        
        animation.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation arg0) {
				
				
			}
			
			@Override
			public void onAnimationRepeat(Animation arg0) {
				
				
			}
			
			@Override
			public void onAnimationEnd(Animation arg0) {
				
				
		        details1.setVisibility(View.VISIBLE);
		        details3.setVisibility(View.VISIBLE);
		        details2.setVisibility(View.VISIBLE);
			}
		});
        
        
        rootView.setOnKeyListener( new OnKeyListener(){
            @Override
            public boolean onKey( View v, int keyCode, KeyEvent event ){
                if( keyCode == KeyEvent.KEYCODE_BACK ){
                	
                	Log.i("Bakc","Pressed");
                	
                	if (fragmentStack.size() == 2) {
                	    FragmentTransaction ft = getFragmentManager().beginTransaction();
                	    fragmentStack.lastElement().onPause();
                	    ft.remove(fragmentStack.pop());
                	    fragmentStack.lastElement().onResume();
                	    ft.show(fragmentStack.lastElement());
                	    ft.commit();
                	} 
                	
                	
                    return true;
                }
                return false;
            }
        } );
        
        return rootView;
    }




	@Override
	public void backButtonPressed() {

    	Log.i("Bakc","Pressed");
    	Log.i("StackSize",""+fragmentStack.size());
    	if (fragmentStack.size() == 2) {
    		
    	    FragmentTransaction ft = getFragmentManager().beginTransaction();
    	    fragmentStack.lastElement().onPause();
    	    ft.remove(fragmentStack.pop());
    	    fragmentStack.lastElement().onResume();
    	    ft.show(fragmentStack.lastElement());
    	    ft.commit();
    	}
    	else
    	{
    		this.getActivity().onBackPressed();
    	}
		
	}
	
	
}
