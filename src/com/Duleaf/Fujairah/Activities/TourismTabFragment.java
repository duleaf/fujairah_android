package com.Duleaf.Fujairah.Activities;

import java.util.Stack;

import com.Duleaf.Fujairah.Adapters.BackButton;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TourismTabFragment extends Fragment implements BackButton {

	
	private FragmentTabHost mTabHost;
	
	public static Stack<Fragment> places;
	public static Stack<Fragment> plans;
	
	static String TAG2 = "SEARCH";
	static String TAG1 = "PLAN";
	
	
	//Mandatory Constructor
    public TourismTabFragment() {
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.news_tabs, container, false);
         
        
        
        mTabHost = (FragmentTabHost)rootView.findViewById(android.R.id.tabhost);
        mTabHost.setup(getActivity().getApplicationContext(), getChildFragmentManager(), R.id.realtabcontent);

        mTabHost.addTab(mTabHost.newTabSpec(TAG1).setIndicator("Plan"),
                PlanFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec(TAG2).setIndicator("Search"),
        		PlaceTap.class, null);
        
        places = new Stack<Fragment>();
        plans = new Stack<Fragment>();
        
        
        return rootView;
	}

	@Override
	public void backButtonPressed() {

		if(mTabHost.getCurrentTab() == 0)
		{
			Log.i("Bakc","Pressed");
	    	Log.i("StackSize",""+plans.size());
	    	if (plans.size() == 2) {
	    		
	    	    FragmentTransaction ft = getFragmentManager().beginTransaction();
	    	    plans.lastElement().onPause();
	    	    ft.remove(plans.pop());
	    	    plans.lastElement().onResume();
	    	    ft.show(plans.lastElement());
	    	    ft.commit();
	    	}
	    	else
	    	{
	    		this.getActivity().onBackPressed();
	    	}
		}
		
		else
		{
			Log.i("Bakc","Pressed");
	    	Log.i("StackSize",""+places.size());
	    	if (places.size() >= 2) {
	    		
	    	    FragmentTransaction ft = getFragmentManager().beginTransaction();
	    	    places.lastElement().onPause();
	    	    ft.remove(places.pop());
	    	    places.lastElement().onResume();
	    	    ft.show(places.lastElement());
	    	    ft.commit();
	    	}
	    	else
	    	{
	    		this.getActivity().onBackPressed();
	    	}
		}
		
	}
	

}
