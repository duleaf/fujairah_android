package com.Duleaf.Fujairah.Activities;

import com.Duleaf.Fujairah.Adapters.BackButton;
import com.Duleaf.Fujairah.Adapters.LangaugeController;
import com.Duleaf.Fujairah.Model.FujConstants;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.LayoutDirection;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

@SuppressLint("NewApi")
public class HistoryOfFujFragment extends Fragment implements BackButton {

	
	TextView title;
	TextView desc;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.history_of_fujairah, container, false);
         
        
        title = (TextView) rootView.findViewById(R.id.textView1);
        desc  = (TextView) rootView.findViewById(R.id.textView2);
        
        if(LangaugeController.getCurrentLangauge().equalsIgnoreCase(FujConstants.AR_Lang))
        {
        	title.setTextAlignment(TextView.TEXT_ALIGNMENT_TEXT_END);
        	title.setLayoutDirection(LayoutDirection.RTL);
        	desc.setTextAlignment(TextView.TEXT_ALIGNMENT_TEXT_END);
        	desc.setLayoutDirection(LayoutDirection.RTL);
        }
        
        
        return rootView;
    }

	@Override
	public void backButtonPressed() {
		// TODO Auto-generated method stub
		
	}
	
}
