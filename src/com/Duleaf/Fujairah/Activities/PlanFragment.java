package com.Duleaf.Fujairah.Activities;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.Duleaf.Fujairah.Adapters.PlanListAdapter;
import com.Duleaf.Fujairah.Model.Plan;

public class PlanFragment extends Fragment {
	
	ListView listView;

	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.plan_fragment, container, false);
   
        ArrayList<Plan> plans = new ArrayList<Plan>();
		
		PlanListAdapter planAdapter = new PlanListAdapter(getActivity(), plans);
		listView = (ListView) rootView.findViewById(R.id.listView1);
		
		listView.setAdapter(planAdapter);
        
        return rootView;
    }

}
