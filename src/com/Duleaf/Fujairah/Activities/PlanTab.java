package com.Duleaf.Fujairah.Activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class PlanTab extends FragmentActivity {

	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.place_tab);
		
		
		android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
	    android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
	    PlaceFragment fragment = new PlaceFragment();
	    fragmentTransaction.replace(R.id.place_container, fragment);
	    fragmentTransaction.commit();
		
	}
	
}
