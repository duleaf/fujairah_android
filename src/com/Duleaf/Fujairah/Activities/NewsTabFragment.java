package com.Duleaf.Fujairah.Activities;

import java.util.Observable;
import java.util.Observer;

import com.Duleaf.Fujairah.DB.DatabaseHelper;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class NewsTabFragment extends Fragment {

	
	private FragmentTabHost mTabHost;
	
	//Mandatory Constructor
    public NewsTabFragment() {
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
		
		
        View rootView = inflater.inflate(R.layout.news_tabs, container, false);
         
        mTabHost = (FragmentTabHost)rootView.findViewById(android.R.id.tabhost);
        mTabHost.setup(getActivity().getApplicationContext(), getChildFragmentManager(), R.id.realtabcontent);

        mTabHost.addTab(mTabHost.newTabSpec("Social").setIndicator("Social"),
                Social.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("News").setIndicator("News"),
        		NewsFragment.class, null);
        
    
        
        return rootView;
	}

}