package com.Duleaf.Fujairah.Activities;

import android.app.Application;
import android.text.TextUtils;

import com.Duleaf.Fujairah.Update.UpdateManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.instabug.library.Instabug;
import com.instabug.wrapper.impl.v14.InstabugAnnotationActivity;

public class FujairahApplication extends Application {
	
	public static final String TAG = FujairahApplication.class.getSimpleName();
	  
    private RequestQueue mRequestQueue;
  
    private static FujairahApplication mInstance;
    public UpdateManager updateManager;
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
//		Instabug.initialize(this)
//        .setAnnotationActivityClass(InstabugAnnotationActivity.class)
//        .setShowIntroDialog(true)
//        .setEnableOverflowMenuItem(true);
		updateManager = new UpdateManager();
	}

	 public static synchronized FujairahApplication getInstance() {
	        return mInstance;
	    }
	  
	    public RequestQueue getRequestQueue() {
	        if (mRequestQueue == null) {
	            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
	        }
	  
	        return mRequestQueue;
	    }
	  
	    public <T> void addToRequestQueue(Request<T> req, String tag) {
	        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
	        getRequestQueue().add(req);
	    }
	  
	    public <T> void addToRequestQueue(Request<T> req) {
	        req.setTag(TAG);
	        getRequestQueue().add(req);
	    }
	  
	    public void cancelPendingRequests(Object tag) {
	        if (mRequestQueue != null) {
	            mRequestQueue.cancelAll(tag);
	        }
	    }
	
}
