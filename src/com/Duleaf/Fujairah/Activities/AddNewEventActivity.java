package com.Duleaf.Fujairah.Activities;

import java.util.Date;
import java.util.TimeZone;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.Duleaf.Fujairah.DB.DatabaseHelper;
import com.Duleaf.Fujairah.Model.Place;
import com.Duleaf.Fujairah.Model.Plan;

public class AddNewEventActivity extends Fragment {

	
	TextView title;
	TimePicker timePicker;
	DatePicker datePicker;
	Button createEvent;
	ContentValues event;
	Plan plan;
	Place place;
	DatabaseHelper db;
	
	

		@Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
	 
	        View rootView = inflater.inflate(R.layout.activity_add_new_event, container, false);
	    
			
		
		
		title = (TextView) rootView.findViewById(R.id.title);
		timePicker = (TimePicker) rootView.findViewById(R.id.timePicker1);
		datePicker = (DatePicker) rootView.findViewById(R.id.datePicker1);
		createEvent = (Button) rootView.findViewById(R.id.create_event);
		
		db = new DatabaseHelper(getActivity());
		plan = new Plan();
		
		title.setText("Wave Hotel");
		
		event = new ContentValues();

		
		
		
		
		createEvent.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				event.put("calendar_id", "1");
				event.put("title", "Wave Hotel");
				event.put("description", "going to "+ "Wave Hotel");
//				event.put("eventLocation", "Event Location");
				
				long startTime = System.currentTimeMillis();
				
				Date end = new Date(datePicker.getYear(), datePicker.getMonth(),
						datePicker.getDayOfMonth(), timePicker.getCurrentHour(), timePicker.getCurrentMinute());
				
				
				long endTime = end.getTime();
				event.put("dtstart", startTime);
				event.put("dtend", endTime);
				event.put("eventTimezone",""+TimeZone.getDefault());
				
				Uri eventUri;
				if (android.os.Build.VERSION.SDK_INT <= 7 )
				{
				  //the old way
				  eventUri    = Uri.parse("content://calendar/events");
				}
				else
				{
				 //the new way
				 eventUri    = Uri.parse("content://com.android.calendar/events");
				}
				
				  Uri url =getActivity().getContentResolver().insert(eventUri, event);
				  
				  plan.place = place;
				  plan.setDate( ""+end);
				  db.createNewPlane(plan);
				  
			}
		});
		return rootView;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
}
