package com.Duleaf.Fujairah.Activities;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.Duleaf.Fujairah.Adapters.CategoriesListAdapter;
import com.Duleaf.Fujairah.Adapters.CatsListOfListAdapter;
import com.Duleaf.Fujairah.Model.Place;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class CategoriesFragment extends Fragment {
    private Place place;
    private GoogleMap mMap;
    private SupportMapFragment fragment;
    private Bundle mBundle;
    CatsListOfListAdapter categories;
    ListView listV;
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.categories_list, container, false);
    
		categories = new CatsListOfListAdapter(getActivity(), null);
		listV = (ListView) rootView.findViewById(R.id.cat_list);
		
		listV.setAdapter(categories);
        
        
        listV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				CategoriesDetailsFragment newFragment = new CategoriesDetailsFragment();
				FragmentTransaction transaction = getFragmentManager().beginTransaction();

				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.add(R.id.place_container, newFragment);
				transaction.hide(TourismTabFragment.places.lastElement());
				transaction.addToBackStack(null);

				// Commit the transaction
				transaction.commit();
				TourismTabFragment.places.push(newFragment);

			}
		});
	      
        
        return rootView;
	}
	
	
	 @Override
	    public void onActivityCreated(Bundle savedInstanceState) {
	        super.onActivityCreated(savedInstanceState);
	      
	        if (mMap == null) {
	        	
		    	   FragmentManager fm = getChildFragmentManager();
		           fragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
		           if (fragment == null) {
		               fragment = SupportMapFragment.newInstance();
		               fm.beginTransaction().replace(R.id.map_container, fragment).commit();
		           }
		        	mMap = fragment.getMap();//((SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
		 
		            // check if map is created successfully or not
		            if (mMap == null) {
//		                Toast.makeText(this,
//		                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
//		                        .show();
		            }
		        }

				Log.i("Map","Map View");

				place = new Place();
			        setUpMapIfNeeded();
			        mBundle = savedInstanceState;
	    }

	    @Override
	    public void onResume() {
	        super.onResume();
	        setUpMapIfNeeded();
	    }
	
	public void onDestroyView() 
    {
       super.onDestroyView(); 
       FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
       ft.remove(fragment);
       ft.commit();
   }
	
	@Override
	public void onPause(){
	super.onPause();
	if(mMap != null) {   
		mMap = null;
	}
	}

	void drawMap()
	{

	        setUpMap();
//	        mBundle = savedInstanceState;
	}
	
	   private void setUpMapIfNeeded() {
		   
		   
	            if (mMap != null) 
	                setUpMap();
	        
	   }
	
	 private void setUpMap() {

		    
			Log.i("Map","Map Setup");

			if (place != null) 
	    	{
	    		
				Log.i("Map","Map Intialized");
				
					place.setLang ("55.1629823");
					place.setLat  ("25.1138498");
					
	    			
	    	    	MarkerOptions marker = new MarkerOptions().position(new LatLng(Double.parseDouble(place.getLat()), Double.parseDouble(place.getLang()))).
	    	    			title(place.getTitle());
	   	    	 
	    	    	// Changing marker icon
//	    	    	marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
	    	    	 
//	    	    	int zoom = (int) Math.floor(8 - Math.log(1.6446 * location.getDistance() / Math.sqrt(2 * (location.getDistance()))) / Math.log (2));
	    	    	
//	    	    	CameraPosition cameraPosition = new CameraPosition.Builder().target(
//	    	                new LatLng((Double.parseDouble(Singlton.getInstance().currentLocation.getLang()) + Double.parseDouble( location.getLang()) )/2,
//	    	                		(Double.parseDouble(Singlton.getInstance().currentLocation.getLat()) + Double.parseDouble( location.getLat()) )/2)).zoom(zoom).build();
	    	    	
	    	    	mMap.setMyLocationEnabled(true);
//	    	    	mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));	
	    	
	    	    	// adding marker
	    	    	mMap.addMarker(marker);	
	    		
				
			}

	    
	    }
	    
	
}
