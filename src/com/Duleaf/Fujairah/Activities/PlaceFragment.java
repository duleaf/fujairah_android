package com.Duleaf.Fujairah.Activities;


import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.Duleaf.Fujairah.Adapters.CategoriesListAdapter;
import com.Duleaf.Fujairah.DB.DatabaseHelper;
import com.Duleaf.Fujairah.Model.Artical;
import com.Duleaf.Fujairah.Model.PlaceCategory;

public class PlaceFragment extends Fragment implements Observer{

	CategoriesListAdapter categories;
	ListView listV;
	ArrayList<PlaceCategory> cats;
	DatabaseHelper dbManager;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
		FujairahApplication app = (FujairahApplication)(getActivity().getApplicationContext());
		app.updateManager.addObserver(this);
		dbManager = new DatabaseHelper(getActivity());
		
        View rootView = inflater.inflate(R.layout.placefragment, container, false);

		TourismTabFragment.places.push(this);

		cats = dbManager.getAllCategories();
        
       Log.i("OnCreate", "OnCreate");
		categories = new CategoriesListAdapter(getActivity(), cats);
		listV = (ListView) rootView.findViewById(R.id.listView1);
		
		listV.setAdapter(categories);
		
		listV.setOnItemClickListener(new OnItemClickListener() {


			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) 
			{
				CategoriesFragment newFragment = new CategoriesFragment();
				FragmentTransaction transaction = getFragmentManager().beginTransaction();

				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.add(R.id.place_container, newFragment);
				transaction.hide(TourismTabFragment.places.lastElement());
				transaction.addToBackStack(null);

				// Commit the transaction
				transaction.commit();
				TourismTabFragment.places.push(newFragment);

				
			}
		});
        
        return rootView;
    }
	
	@Override
	public void update(Observable observable, Object data) {
		Log.i("UPdate","Notification");
		
		cats = dbManager.getAllCategories();
		categories.notifyDataSetChanged();
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		FujairahApplication app = (FujairahApplication)(getActivity().getApplicationContext());
		app.updateManager.deleteObserver(this);
	}
	
	
}
