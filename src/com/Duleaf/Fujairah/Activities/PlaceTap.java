package com.Duleaf.Fujairah.Activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PlaceTap extends Fragment {

	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.place_tab, container, false);

		
		android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
	    android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
	    PlaceFragment fragment = new PlaceFragment();
	    fragmentTransaction.replace(R.id.place_container, fragment);
	    fragmentTransaction.commit();
		return rootView;
	}
	

}
