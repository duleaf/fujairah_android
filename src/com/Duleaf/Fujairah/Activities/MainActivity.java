package com.Duleaf.Fujairah.Activities;

import java.util.ArrayList;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.Duleaf.Fujairah.Adapters.BackButton;
import com.Duleaf.Fujairah.Adapters.LangaugeController;
import com.Duleaf.Fujairah.Adapters.NavDrawerListAdapter;
import com.Duleaf.Fujairah.Model.FujConstants;
import com.Duleaf.Fujairah.Model.NavDrawerItem;

@SuppressLint({ "NewApi", "CommitTransaction" })
public class MainActivity extends FragmentActivity {

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private Button toggle;
	BackButton currentFragment;
//	private ActionBarDrawerToggle mDrawerToggle;
	
	//currentGravity
	int currentGravity;
	TextView title;
	// nav drawer title
		private CharSequence mDrawerTitle;
	
	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		String languageToLoad = LangaugeController.getCurrentLangauge() ; // your language
	    Locale locale = new Locale(languageToLoad); 
	    Locale.setDefault(locale);
	    Configuration config = new Configuration();
	    config.locale = locale;
	    getBaseContext().getResources().updateConfiguration(config, 
	      getBaseContext().getResources().getDisplayMetrics());
		
		setContentView(R.layout.activity_main);
		
		
		ActionBar actionBar = getActionBar();
	    actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
	    actionBar.setCustomView(R.layout.actionbar);
	    actionBar.setHomeButtonEnabled(false);
	    actionBar.setDisplayShowTitleEnabled(false);
	    actionBar.setDisplayHomeAsUpEnabled(false);
	    actionBar.setDisplayUseLogoEnabled(false);
	    actionBar.setDisplayShowHomeEnabled(false);
	    actionBar.setDisplayShowCustomEnabled(true);
	    
	    title = (TextView) actionBar.getCustomView().findViewById(R.id.bar_title);
	    
	    title.setText("Welcome to mFujairah");
	    
	    
	    
	    if(LangaugeController.getCurrentLangauge().equalsIgnoreCase(FujConstants.AR_Lang))
	    {
	    	title.setText("مرحبا في تطبيق الفجيرة");
		    toggle = (Button) actionBar.getCustomView().findViewById(R.id.left_button);
	    	toggle.setVisibility(View.INVISIBLE);
	    	toggle = (Button) actionBar.getCustomView().findViewById(R.id.right_button);
	    }
	    else
	    {
		    toggle = (Button) actionBar.getCustomView().findViewById(R.id.right_button);
	    	toggle.setVisibility(View.INVISIBLE);
	    	toggle = (Button) actionBar.getCustomView().findViewById(R.id.left_button);
	    }
	    
		
		currentGravity = Gravity.LEFT;
		
		if(LangaugeController.getCurrentLangauge().equalsIgnoreCase(FujConstants.AR_Lang))
		{
			currentGravity = Gravity.RIGHT;
		}
		
		mTitle = mDrawerTitle = getTitle();
		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
		// nav drawer icons from resources
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		mDrawerList = (ListView) findViewById(R.id.list_slidermenu_en);
		mDrawerList.setLayoutDirection(LayoutDirection.LTR);
		mDrawerLayout.setLayoutDirection(LayoutDirection.LTR);
		
		
		if(LangaugeController.getCurrentLangauge().equalsIgnoreCase(FujConstants.AR_Lang))
		{
			mDrawerLayout.setLayoutDirection(LayoutDirection.RTL);
			mDrawerList = (ListView) findViewById(R.id.list_slidermenu_ar);
			mDrawerList.setLayoutDirection(LayoutDirection.RTL);
		}
		
		
		navDrawerItems = new ArrayList<NavDrawerItem>();
		
		for(int counter = 0 ; counter < 8 ; counter ++)
		{
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[counter], navMenuIcons.getResourceId(counter, -1)));			
		}
		
		// Recycle the typed array
		navMenuIcons.recycle();

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		
		

//		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
//				R.drawable.menu_button, //nav menu toggle icon
//				R.string.app_name, // nav drawer open - description for accessibility
//				R.string.app_name // nav drawer close - description for accessibility
//		) {
//			
//			
//			public void onDrawerClosed(View view) {
//				getActionBar().setTitle(mTitle);
//				
//				// calling onPrepareOptionsMenu() to show action bar icons
//				invalidateOptionsMenu();
//			}
//
//			public void onDrawerOpened(View drawerView) {
//				getActionBar().setTitle(mDrawerTitle);
//				// calling onPrepareOptionsMenu() to hide action bar icons
//				invalidateOptionsMenu();
//			}
//			
//	        @Override
//	        public boolean onOptionsItemSelected(MenuItem item) {
//	            if (item != null && item.getItemId() == android.R.id.home) {
//	                if (mDrawerLayout.isDrawerOpen(currentGravity)) {
//	                    mDrawerLayout.closeDrawer(currentGravity);
//	                } else {
//	                    mDrawerLayout.openDrawer(currentGravity);
//	                }
//	            }
//	            return false;
//	        }
//		};
//		mDrawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
		toggle.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				if (mDrawerLayout.isDrawerOpen(currentGravity)) {
                    mDrawerLayout.closeDrawer(currentGravity);
                } else {
                    mDrawerLayout.openDrawer(currentGravity);
                }
				
			}
		});

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0);
		}

	}
	
	

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
	}
public ArrayList<NavDrawerItem> getMenuItems(){
		
		return navDrawerItems;
	}
public  void settMenuItems(ArrayList<NavDrawerItem> items){
	
	 navDrawerItems = items;
}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (item != null && item.getItemId() == android.R.id.home) {
	        if (mDrawerLayout.isDrawerOpen(currentGravity)) {
	            mDrawerLayout.closeDrawer(currentGravity);
	        } else {
	            mDrawerLayout.openDrawer(currentGravity);
	        }
	    }
//		if (mDrawerToggle.onOptionsItemSelected(item)) {
//			return true;
//		}
		// Handle action bar actions click
		switch (item.getItemId()) {
		case R.id.action_settings:
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	private void displayView(int position) {
		// update the main content by replacing fragments
		
		title.setText("");
		
		Fragment fragment = null;
		switch (position) {
		case 0:
			fragment = new HomeFragment();
			
		    title.setText("Welcome to mFujairah");
		   
		    if(LangaugeController.getCurrentLangauge().equalsIgnoreCase(FujConstants.AR_Lang))
		    	title.setText("مرحبا في تطبيق الفجيرة");
		    
			break;
		case 1:
			fragment = new FujRulerFragment();
			break;
		case 2:
			fragment = new AboutEmaratFragment();
			
			break;
		case 3:
			fragment = new HistoryOfFujFragment();
			break;
		case 4:
			fragment = new NewsTabFragment();
	
			break;
		case 5:
			fragment = new FujGovFragment();
			break;
		case 6:
			fragment = new TourismTabFragment();
			break;
		case 7:
			fragment = new CallUsFragment();
			break;
			

		default:
			break;
		}
		
		if (fragment != null) {
			
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.frame_container, fragment).commit();

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
			if(BackButton.class.isAssignableFrom(fragment.getClass()))
				currentFragment =(BackButton) fragment;
			else
				currentFragment = null;
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

//	@Override
//	protected void onPostCreate(Bundle savedInstanceState) {
//		super.onPostCreate(savedInstanceState);
//		// Sync the toggle state after onRestoreInstanceState has occurred.
//		mDrawerToggle.syncState();
//	}

//	@Override
//	public void onConfigurationChanged(Configuration newConfig) {
//		super.onConfigurationChanged(newConfig);
//		newConfig.setLayoutDirection(Locale.getDefault());
//
//		// Pass any configuration change to the drawer toggls
//		mDrawerToggle.onConfigurationChanged(newConfig);
//	}
	@Override
	 public boolean onKeyDown(int keyCode, KeyEvent event)
	 {
	 if (keyCode == KeyEvent.KEYCODE_BACK)
	 {
		 Log.i("BaCK","Pressed");
			if(currentFragment != null)
			currentFragment.backButtonPressed();
		 
//	    if (getFragmentManager().getBackStackEntryCount() == 0)
//	    {
//	        this.finish();
//	        return false;
//	    }
//	    else
//	    {
//	        getFragmentManager().popBackStack();
//	        removeCurrentFragment();
//
//	        return false;
//	    }



	 }

	  return super.onKeyDown(keyCode, event);
	 }


	public void removeCurrentFragment()
	{
	FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

	Fragment currentFrag =  getSupportFragmentManager().findFragmentById(R.id.frame_container);
	
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Log.i("Big BACK","BACKKK");
	}


}
