package com.Duleaf.Fujairah.Activities;

import java.util.Date;
import java.util.TimeZone;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.Duleaf.Fujairah.Adapters.LangaugeController;
import com.Duleaf.Fujairah.DB.DatabaseHelper;
import com.Duleaf.Fujairah.Model.FujConstants;
import com.Duleaf.Fujairah.Model.Place;
import com.Duleaf.Fujairah.Model.Plan;

public class NewEventFragment extends Fragment {

	public Place place;
	EditText title;
	TimePicker timePicker;
	DatePicker datePicker;
	Button createEvent;
	ContentValues event;
	Plan plan;
	DatabaseHelper db;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		View rootView;
		
		if(LangaugeController.getCurrentLangauge().equalsIgnoreCase(FujConstants.AR_Lang))
			rootView = inflater.inflate(R.layout.activity_add_new_event_ar, container, false);
		else
			rootView = inflater.inflate(R.layout.activity_add_new_event, container, false);
        
		title = (EditText) rootView.findViewById(R.id.title);
		timePicker = (TimePicker) rootView.findViewById(R.id.timePicker1);
		datePicker = (DatePicker) rootView.findViewById(R.id.datePicker1);
		createEvent = (Button) rootView.findViewById(R.id.create_event);
        
		db = new DatabaseHelper(getActivity());
		plan = new Plan();
		title.setText(place.getTitle());
		event = new ContentValues();
		
		
		
createEvent.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				event.put("calendar_id", "1");
				event.put("title", place.getTitle());
				event.put("description", "going to "+ place.getTitle());
//				event.put("eventLocation", "Event Location");
				
				long startTime = System.currentTimeMillis();
				
				Date end = new Date(datePicker.getYear(), datePicker.getMonth(),
						datePicker.getDayOfMonth(), timePicker.getCurrentHour(), timePicker.getCurrentMinute());
				
				
				long endTime = end.getTime();
				event.put("dtstart", startTime);
				event.put("dtend", endTime);
				event.put("eventTimezone",""+TimeZone.getDefault());
				
				Uri eventUri;
				if (android.os.Build.VERSION.SDK_INT <= 7 )
				{
				  //the old way
				  eventUri    = Uri.parse("content://calendar/events");
				}
				else
				{
				 //the new way
				 eventUri    = Uri.parse("content://com.android.calendar/events");
				}
				
				  Uri url = getActivity().getContentResolver().insert(eventUri, event);
				  
				  plan.place = place;
				  plan.setDate( ""+end);
				  db.createNewPlane(plan);
				  
			}
		});

        
        return rootView;
	}

}
