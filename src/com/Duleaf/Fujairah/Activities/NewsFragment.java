package com.Duleaf.Fujairah.Activities;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.Duleaf.Fujairah.Adapters.LangaugeController;
import com.Duleaf.Fujairah.Adapters.NewsListAdapter;
import com.Duleaf.Fujairah.DB.DatabaseHelper;
import com.Duleaf.Fujairah.Model.Artical;
import com.Duleaf.Fujairah.Model.FujConstants;

public class NewsFragment  extends Fragment  implements Observer {


	ListView listView;
	NewsListAdapter adapter;
	DatabaseHelper dbManager;
	ArrayList<Artical> arts;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
		FujairahApplication app = (FujairahApplication)(getActivity().getApplicationContext());
		app.updateManager.addObserver(this);
		dbManager = new DatabaseHelper(getActivity());
		
        View rootView = inflater.inflate(R.layout.news, container, false);
        
        listView = (ListView) rootView.findViewById(R.id.listView1);
        
         arts = dbManager.getAllArticals();
        
//        if(LangaugeController.getCurrentLangauge().equalsIgnoreCase(FujConstants.EN_Lang))
//        {
//            Artical art = new Artical("Pedestrian bridge is need of the hour", "", "", "", "October 28, 2014");
//            arts.add(art);
//            
//            art = new Artical("Readers raise issues with companies", "", "", "", "October 28, 2014");
//            arts.add(art);
//            
//            art = new Artical("ADFF: Do films have an impact on children?", "", "", "", "October 28, 2014");
//            arts.add(art);
//           
//            art = new Artical("Maid accused of forcing children to eat at knifepoint", "", "", "", "October 19, 2014");
//            arts.add(art);
//            
//            art = new Artical("Abu Dhabi Strategic Debate forum opens", "", "", "", "October 19, 2014");
//            arts.add(art);
//            
//            art = new Artical("Umm Al Quwain CID arrest gang for e-mail fraud", "", "", "", "October 19, 2014");
//            arts.add(art);
//            
//            art = new Artical("Readers raise issues with companies", "", "", "", "October 28, 2014");
//            arts.add(art);
//            
//            art = new Artical("ADFF: Do films have an impact on children?", "", "", "", "October 28, 2014");
//            arts.add(art);
//           
//            art = new Artical("Maid accused of forcing children to eat at knifepoint", "", "", "", "October 19, 2014");
//            arts.add(art);
//        }
//        else
//        {
//            Artical art = new Artical("القضاء البريطاني يسمح لامرأة بإنهاء حياة ابنتها المعاقة", "", "", "", "October 28, 2014");
//            arts.add(art);
//            
//            art = new Artical("شرطة دبي وهيئة الطرق والمواصلات تبحثان إدارة التداخل في التقاطعات", "", "", "", "October 28, 2014");
//            arts.add(art);
//            
//            art = new Artical("ميسي ورونالدو و6 ألمان في قائمة الفيفا لمرشحي الكرة الذهبية", "", "", "", "October 28, 2014");
//            arts.add(art);
//            
//            art = new Artical("إعتماد 208 طلبات أراض لأهالي إمارة الشارقة", "", "", "", "October 19, 2014");
//            arts.add(art);
//            
//            art = new Artical("محمد بن راشد يطلق الاستراتيجية الوطنية للابتكار", "", "", "", "October 19, 2014");
//            arts.add(art);
//            
//            art = new Artical("أشياء من الماضي", "", "", "", "October 19, 2014");
//            arts.add(art);
//            
//            art = new Artical("شرطة دبي وهيئة الطرق والمواصلات تبحثان إدارة التداخل في التقاطعات", "", "", "", "October 28, 2014");
//            arts.add(art);
//            
//            art = new Artical("ميسي ورونالدو و6 ألمان في قائمة الفيفا لمرشحي الكرة الذهبية", "", "", "", "October 28, 2014");
//            arts.add(art);
//            
//            art = new Artical("إعتماد 208 طلبات أراض لأهالي إمارة الشارقة", "", "", "", "October 19, 2014");
//            arts.add(art);            
//        }
        
        adapter = new NewsListAdapter(getActivity(), arts);
        listView.setAdapter(adapter);
        
        return rootView;
    }


	@Override
	public void update(Observable observable, Object data) {
		Log.i("UPdate","Notification");
		
		arts = dbManager.getAllArticals();
		adapter.notifyDataSetChanged();
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		FujairahApplication app = (FujairahApplication)(getActivity().getApplicationContext());
		app.updateManager.deleteObserver(this);
	}
	
}