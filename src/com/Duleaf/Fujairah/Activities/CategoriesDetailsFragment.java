package com.Duleaf.Fujairah.Activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.Duleaf.Fujairah.Model.Place;

public class CategoriesDetailsFragment extends Fragment {

	Place place;
	private Button addToPlane;
	private TextView description;
	private ImageView imageV;
	
	Button phone;
	Button fb;
	Button location;
	Button tw;
	Button web;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.categories_details, container, false);
    
        
        
        imageV = (ImageView) rootView.findViewById(R.id.imageView1);
		
		
		addToPlane  = (Button) rootView.findViewById(R.id.add_to_cal);
		
		final Context context = getActivity();
		
		final CategoriesDetailsFragment obj = this;
		
		addToPlane.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
//
//				Intent intent = new Intent(getActivity(), AddNewEventActivity.class);
//				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//				startActivity(intent);
				AddNewEventActivity newFragment = new AddNewEventActivity();
				FragmentTransaction transaction = getFragmentManager().beginTransaction();

				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.fragment_container, newFragment);
				transaction.addToBackStack(null);

				// Commit the transaction
				transaction.commit();
				TourismTabFragment.places.push(newFragment);

				
				
			}
		});
		
		
		 phone = (Button) rootView.findViewById(R.id.phone);
	        fb = (Button) rootView.findViewById(R.id.fb);
	        location = (Button) rootView.findViewById(R.id.location);
	        tw = (Button) rootView.findViewById(R.id.tw);
	        web = (Button) rootView.findViewById(R.id.web);
	        
	        
	        phone.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					
					String uri = "tel:" +"00928495783" ;
					 Intent intent = new Intent(Intent.ACTION_CALL);
					 intent.setData(Uri.parse(uri));
					 getActivity().startActivity(intent);
					
				}
			});
	        
	        
	        web.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.fujmun.gov.ae/"));
					startActivity(browserIntent);
				}
			});
	        
	        fb.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					
					Intent browserIntent ;
						   try {
						    context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
						    browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/<id_here>"));
						   } catch (Exception e) {
							   browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/<user_name_here>"));
						   }
						
					
					// new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/FujairahMunicipality"));
					startActivity(browserIntent);
					
				}
			});
	        
	        
	        location.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					
				}
			});
	        
	        
	        tw.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
						
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/fujmun"));
					startActivity(browserIntent);
					
				}
			});
	        
		
		
        return rootView;
	}
		
		


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */


}
