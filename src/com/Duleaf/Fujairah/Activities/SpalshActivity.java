package com.Duleaf.Fujairah.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.Duleaf.Fujairah.Adapters.LangaugeController;
import com.Duleaf.Fujairah.Model.FujConstants;

public class SpalshActivity extends Activity {

	ImageView  arImage;
	ImageView  enImage;
	ImageView  LangHolderImage;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_spalsh);
		
		arImage = (ImageView) findViewById(R.id.ar_img);
		enImage = (ImageView) findViewById(R.id.en_img);
		LangHolderImage = (ImageView) findViewById(R.id.lang);

		
		arImage.setVisibility(View.INVISIBLE);
		enImage.setVisibility(View.INVISIBLE);
		LangHolderImage.setVisibility(View.INVISIBLE);
		
		final Context context = this;
		
		arImage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				LangaugeController.setCurrentLangauge(FujConstants.AR_Lang);
				
				 FujairahApplication app = (FujairahApplication)(getApplicationContext());
				 app.updateManager.context = context;
					app.updateManager.updateAllSections();
				
				Intent i = new Intent(SpalshActivity.this, MainActivity.class);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		        startActivity(i);
		        
		       
				
			}
		});
		
		
		enImage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				LangaugeController.setCurrentLangauge(FujConstants.EN_Lang);
				
				FujairahApplication app = (FujairahApplication)(getApplicationContext());
				app.updateManager.context = context;
				app.updateManager.updateAllSections();
				
				Intent i = new Intent(SpalshActivity.this, MainActivity.class);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		        startActivity(i);
		        
			}
		});
		
		
		new CountDownTimer(2000, 1000) {

		     public void onTick(long millisUntilFinished) {

		    	 LangHolderImage.setVisibility(View.VISIBLE);
		     }

		     public void onFinish() {
		    	 
		    	arImage.setVisibility(View.VISIBLE);
		 		enImage.setVisibility(View.VISIBLE);
		    	 
		     }
		  }.start();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.spalsh, menu);
		return true;
	}

}
