package com.Duleaf.Fujairah.Update;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.Duleaf.Fujairah.Adapters.LangaugeController;
import com.Duleaf.Fujairah.DB.DatabaseHelper;
import com.Duleaf.Fujairah.Model.Artical;
import com.Duleaf.Fujairah.Model.FujConstants;
import com.Duleaf.Fujairah.Model.FujObject;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class ArticalUpdateController extends UpdateController {

	
protected Boolean isNewUpdateAvailable(){
		
		return true;
	}


	protected void updateCurrentDataBase()
	{
		
	}
	
	protected void fetchAllData()
	{
		
		Log.i("Data", "fetchAllData");
		
		String extranlUri = "";
		
		if(LangaugeController.getCurrentLangauge().equalsIgnoreCase(FujConstants.AR_Lang))
			extranlUri = "/?lang=ar";
		
		String url = FujConstants.BASE_URL+this.requestUri+extranlUri;
		Log.i("URL", url);
		
		RequestQueue queue = Volley.newRequestQueue(context);

		// Request a string response from the provided URL.
		StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
		            new Response.Listener() {
		 
			@Override
			public void onResponse(Object arg0) {
				
				Log.i("UPdateArt",arg0.toString());
				dbManager.deleteALLArticals();
				ArrayList<FujObject> articals = ParssedObjects(arg0);
				for(FujObject artical : articals)
				{
					Log.i("Insert Object", artical.toString());
					dbManager.createNewArtical((Artical)artical);
				}
				delegate.UpdateFinished();
				
			}
		}, new Response.ErrorListener() {
		    @Override
		    public void onErrorResponse(VolleyError error) {
		    }
		});
		// Add the request to the RequestQueue.
		queue.add(stringRequest);
		
		
	}
	protected ArrayList<FujObject> ParssedObjects(Object response)
	{
		String strResponse =  response.toString();
		ArrayList<FujObject> articals = new ArrayList<FujObject>();

		try {
			JSONObject jsonObj = new JSONObject(strResponse);
			
			JSONArray jsonArray = new JSONArray(jsonObj.getString("posts"));
			
			for (int counter = 0 ; counter < jsonArray.length() ; counter ++) {
				Log.i("Parssing Object","Counter: " + counter);
				JSONObject post = (JSONObject) jsonArray.get(counter);
				int id = post.getInt("id");
				Artical artical = new Artical(post.getString("title"),
						post.getString("content"), "", "", "" , id);
				
				articals.add(artical);
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return articals;
	}
	
	
	public void StartUpdating()
	{
		fujObject = new FujObject();
		dbManager = new DatabaseHelper(context);
		if(this.fujObject.numberOfAvailableRecords() > MAX_NUMBER_OF_RECORDS)
		{
			if(isNewUpdateAvailable())
			{
				
			}
			
			this.delegate.UpdateFinished();
		}
		else
		{
			this.fetchAllData();
		}
		
	}
	
	
}
