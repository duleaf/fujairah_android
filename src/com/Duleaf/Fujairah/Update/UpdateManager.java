package com.Duleaf.Fujairah.Update;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Stack;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

public class UpdateManager extends Observable implements UpdateDelegate {
//	private ArrayList<String[]>sections;
	private BufferedReader br;
	Stack<String[]>sections;
	public Context context;
	String [] currentSection;
	
	
	public void updateAllSections()
	{
		this.loadSections();
		
		
	}
	
	@SuppressWarnings("unchecked")
	void updateNewSection(String [] data)
	{
		this.currentSection = data;
		
		String classTag = data[1];
		Class<?> classObject;
		UpdateController updateController = null;
		try {
			classObject = ((Class<?>) Class.forName(classTag));
			Constructor<?> constructor = classObject.getConstructor();
			 updateController = (UpdateController)constructor.newInstance();
			 updateController.context = context;
			 updateController.requestUri = data[0];
			 updateController.delegate = this;
			 updateController.StartUpdating();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		updateController
	}
	
	void loadSections() 
	{
		AssetManager am = context.getAssets();
		InputStream inputStream = null;
		try {
			inputStream = am.open("services");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			
			e1.printStackTrace();
		}
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		sections = new Stack<String[]>();
		try {
		    br = new BufferedReader(inputStreamReader);
		    String line;
		    int counter = 0;
		    String section[] = new String[3];
		    
		    while ((line = br.readLine()) != null) {
		    	
		    	Log.i("Line", line);
		        
		    	if(counter > 0 && counter %3 == 0)
		        {
		    		sections.push(section);
		    		section = new String[3];
		        }
		    	
		    	section[counter % 3] = line;
		    	counter ++;
		    }
		}
		catch (IOException e) {
		    //You'll need to add proper error handling here
		}
		
		updateNewSection(sections.get(0));
	}

	@Override
	public void UpdateFinished() {

		
		
		
		if(sections.size() < 1)
	        return;
	    
	    if(sections.size() > 1)
	        sections.pop();
	    else
	    	return;
	    
	    triggerObservers();
		currentSection = sections.lastElement();
		updateNewSection(sections.lastElement());
	    
//	     [[NSNotificationCenter defaultCenter] postNotificationName:[currentInfo objectAtIndex:2] object:nil userInfo:nil];
	    
//	    [self updateNewSection:[notificationInfo lastObject]];
		
	}
	
	
	// Create a method to update the Observerable's flag to true for changes and
    // notify the observers to check for a change. These are also a part of the
    // secret sauce that makes Observers and Observables communicate
    // predictably.
    private void triggerObservers() {
 
        setChanged();
        notifyObservers();
    }
}
