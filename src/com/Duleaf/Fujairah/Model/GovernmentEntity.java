package com.Duleaf.Fujairah.Model;

public class GovernmentEntity extends FujObject
{
	
	String name;
	String intro;
	String logo_uri;
	String image;
	String desc;
	String phone;
	String FB;
	String TW;
	String location;
	String webSite_url;
	
	
	
	
	
	/**
	 * @param name
	 * @param intro
	 * @param logo_uri
	 * @param image
	 * @param desc
	 * @param phone
	 * @param fB
	 * @param tW
	 * @param location
	 * @param webSite_url
	 */
	public GovernmentEntity(String name, String intro, String logo_uri,
			String image, String desc, String phone, String fB, String tW,
			String location, String webSite_url) {
		super();
		this.name = name;
		this.intro = intro;
		this.logo_uri = logo_uri;
		this.image = image;
		this.desc = desc;
		this.phone = phone;
		FB = fB;
		TW = tW;
		this.location = location;
		this.webSite_url = webSite_url;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIntro() {
		return intro;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	public String getLogo_uri() {
		return logo_uri;
	}
	public void setLogo_uri(String logo_uri) {
		this.logo_uri = logo_uri;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFB() {
		return FB;
	}
	public void setFB(String fB) {
		FB = fB;
	}
	public String getTW() {
		return TW;
	}
	public void setTW(String tW) {
		TW = tW;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getWebSite_url() {
		return webSite_url;
	}
	public void setWebSite_url(String webSite_url) {
		this.webSite_url = webSite_url;
	}
	
	
}
