package com.Duleaf.Fujairah.Model;

public class Artical extends FujObject{

	String title;
	String Details;
	String image;
	String type;
	String date;
	
	
	
	
	/**
	 * @param title
	 * @param details
	 * @param image
	 * @param type
	 * @param date
	 * @param id
	 */
	public Artical(String title, String details, String image, String type,
			String date , int id) {
		super();
		this.title = title;
		Details = details;
		this.image = image;
		this.type = type;
		this.date = date;
		this.id = id;
	}
	
	
	/**
	 * @param title
	 * @param details
	 * @param image
	 * @param type
	 * @param date
	 */
	public Artical(String title, String details, String image, String type,
			String date) {
		super();
		this.title = title;
		Details = details;
		this.image = image;
		this.type = type;
		this.date = date;
	}
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDetails() {
		return Details;
	}
	public void setDetails(String details) {
		Details = details;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
	
	
}
