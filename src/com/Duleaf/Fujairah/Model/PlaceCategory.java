package com.Duleaf.Fujairah.Model;


public class PlaceCategory extends FujObject {

	String name ;

	/**
	 * @param name
	 */
	public PlaceCategory(String name,int id) {
		super();
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
}
