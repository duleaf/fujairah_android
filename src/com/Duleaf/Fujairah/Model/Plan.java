package com.Duleaf.Fujairah.Model;

public class Plan extends FujObject{

	public Place place;
	int planId;
	String date;
	/**
	 * @param place
	 * @param date
	 */
	public Plan(Place place, String date) {
		super();
		this.place = place;
		this.date = date;
	}
	
	
	public Plan() {
		// TODO Auto-generated constructor stub
	}


	public int getPlanId() {
		return planId;
	}


	public void setPlanId(int planId) {
		this.planId = planId;
	}


	public Place getPlace() {
		return place;
	}
	public void setPlace(Place place) {
		this.place = place;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
	
}
