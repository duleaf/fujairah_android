package com.Duleaf.Fujairah.Model;

public class Place extends FujObject
{
	private String typeId;
	private String lang;
	private String lat;
	private String title;
	private String desc;
	private String icon_url;
	private String cover_img_url;
	private String phone;
	private String email;
	private String fb_url;
	private String tw_url;
	/**
	 * @param lang
	 * @param lat
	 * @param title
	 * @param desc
	 * @param icon_url
	 * @param cover_img_url
	 * @param phone
	 * @param email
	 * @param fb_url
	 * @param tw_url
	 */
	public Place(String lang, String lat, String title, String desc,
			String icon_url, String cover_img_url, String phone, String email,
			String fb_url, String tw_url) {
		super();
		this.lang = lang;
		this.lat = lat;
		this.title = title;
		this.desc = desc;
		this.icon_url = icon_url;
		this.cover_img_url = cover_img_url;
		this.phone = phone;
		this.email = email;
		this.fb_url = fb_url;
		this.tw_url = tw_url;
	}
	
	
	
	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public Place() {
		// TODO Auto-generated constructor stub
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getIcon_url() {
		return icon_url;
	}
	public void setIcon_url(String icon_url) {
		this.icon_url = icon_url;
	}
	public String getCover_img_url() {
		return cover_img_url;
	}
	public void setCover_img_url(String cover_img_url) {
		this.cover_img_url = cover_img_url;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFb_url() {
		return fb_url;
	}
	public void setFb_url(String fb_url) {
		this.fb_url = fb_url;
	}
	public String getTw_url() {
		return tw_url;
	}
	public void setTw_url(String tw_url) {
		this.tw_url = tw_url;
	}
	
	

}
