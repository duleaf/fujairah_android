package com.Duleaf.Fujairah.Adapters;

import java.util.ArrayList;

import com.Duleaf.Fujairah.Activities.R;
import com.Duleaf.Fujairah.Model.Artical;
import com.Duleaf.Fujairah.Model.FujConstants;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CallUsListAdapter extends BaseAdapter {

	
	private Context context;
	private ArrayList<String> info;
	
	
	
	/**
	 * @param context
	 * @param info
	 */
	public CallUsListAdapter(Context context, ArrayList<String> info) {
		super();
		this.context = context;
		this.info = info;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return info.size()/2;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.about_us_en, null);
            
            if(LangaugeController.getCurrentLangauge().equalsIgnoreCase(FujConstants.AR_Lang))
            {
                convertView = mInflater.inflate(R.layout.about_us_ar, null);
            }
            
            convertView.setMinimumHeight(80);
            }


        TextView txtTitle = (TextView) convertView.findViewById(R.id.textView1);
        TextView details = (TextView) convertView.findViewById(R.id.textView2);
        
        if(LangaugeController.getCurrentLangauge().equalsIgnoreCase(FujConstants.AR_Lang))
        {
        	txtTitle.setGravity(Gravity.RIGHT);
        	details.setGravity(Gravity.RIGHT);
        }
        
         
         txtTitle.setText(info.get(position));
         details.setText(info.get(position+1));

         
//        imgIcon.setImageResource(navDrawerItems.get(position).getIcon());        
//        txtTitle.setText(navDrawerItems.get(position).getTitle());

        return convertView;
	}

}
