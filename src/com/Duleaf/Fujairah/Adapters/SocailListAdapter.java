package com.Duleaf.Fujairah.Adapters;

import java.util.ArrayList;

import com.Duleaf.Fujairah.Activities.R;
import com.Duleaf.Fujairah.Model.Artical;
import com.Duleaf.Fujairah.Model.FujConstants;
import com.Duleaf.Fujairah.Model.NavDrawerItem;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SocailListAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<Artical> articals;
	
	
	/**
	 * @param context
	 * @param articals
	 */
	public SocailListAdapter(Context context, ArrayList<Artical> articals) {
		super();
		this.context = context;
		this.articals = articals;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		Log.i("Count : ",""+articals.size());
		return articals.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.social_item, null);
            
//            if(LangaugeController.getCurrentLangauge().equalsIgnoreCase(FujConstants.AR_Lang))
//            {
//                convertView = mInflater.inflate(R.layout.social_item, null);
//            }
//            
            convertView.setMinimumHeight(80);
            }
         
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.imageView1);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.textView1);
        TextView txtdate = (TextView) convertView.findViewById(R.id.textView2);
        
        if(LangaugeController.getCurrentLangauge().equalsIgnoreCase(FujConstants.AR_Lang))
        {
        	txtTitle.setGravity(Gravity.RIGHT);
        	txtdate.setGravity(Gravity.RIGHT);
        }
        
        
         Artical artical = articals.get(position);
         
         txtTitle.setText(artical.getTitle());
         txtdate.setText(artical.getDate());
         
         if(position%2 == 0)
         {
             imgIcon.setImageResource(R.drawable.facebook);        
         }
         else
         {
             imgIcon.setImageResource(R.drawable.twitter);        

         }
         
//        txtTitle.setText(navDrawerItems.get(position).getTitle());

        return convertView;
	}

}


