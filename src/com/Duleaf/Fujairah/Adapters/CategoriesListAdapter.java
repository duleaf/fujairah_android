package com.Duleaf.Fujairah.Adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.Duleaf.Fujairah.Activities.R;
import com.Duleaf.Fujairah.Model.PlaceCategory;

public class CategoriesListAdapter extends BaseAdapter {


	Context context;
	ArrayList<PlaceCategory> topics;
	
	
	/**
	 * @param context
	 * @param topics
	 */
	public CategoriesListAdapter(Context context, ArrayList<PlaceCategory> topics) {
		super();
		this.context = context;
		this.topics = topics;
	}

	public CategoriesListAdapter() {
		
	}
	
	@Override
	public int getCount() {
		Log.i("SIze",""+topics.size());
		return topics.size();
	}

	@Override
	public Object getItem(int arg0) {
		
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		
		return 0;
	}

	@Override
	public View getView(int arg0, View convertView, ViewGroup parent) {
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.category, null);
        }	
		
		TextView txtTitle = (TextView) convertView.findViewById(R.id.textView1);
		
		PlaceCategory cat  = topics.get(arg0);
		txtTitle.setText(cat.getName());
		
		
		return convertView;
	}


}
