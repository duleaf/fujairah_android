package com.Duleaf.Fujairah.Adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.Duleaf.Fujairah.Activities.R;
import com.Duleaf.Fujairah.Model.GovernmentEntity;
import com.squareup.picasso.Picasso;

public class GovernmentGridAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<GovernmentEntity> entities;
 
	public GovernmentGridAdapter(Context context) {
		this.context = context;
	}
	
	public GovernmentGridAdapter(Context context , ArrayList<GovernmentEntity> entities) {
		this.context = context;
		this.entities = entities;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return entities.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int arg0, View convertView, ViewGroup arg2) {
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.gov_item, null);
        }
		TextView txttitle = (TextView) convertView.findViewById(R.id.gov_title);
		
		
		ImageView imageView = (ImageView) convertView
				.findViewById(R.id.gov_img);
		
		GovernmentEntity entity = entities.get(arg0);
		
		Picasso.with(context).load(entity.getLogo_uri()).into(imageView);
		txttitle.setText(entity.getName());
//        
//        if(Singlton.getInstance().currentLang.equalsIgnoreCase("ar"))
//        	txttitle.setText(cat.getName_ar());
//        
//        if(cat.getImagePath().equalsIgnoreCase("")||(cat.getImagePath() == null))
//        	imageView.setImageResource(R.drawable.icon);
		
        return convertView;	
	}

}
