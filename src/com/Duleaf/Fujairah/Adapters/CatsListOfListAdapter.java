package com.Duleaf.Fujairah.Adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.Duleaf.Fujairah.Activities.R;
import com.Duleaf.Fujairah.Model.Place;

public class CatsListOfListAdapter extends BaseAdapter {

	public Context context;
	ArrayList<Place> places;
	
	
	/**
	 * @param contex
	 * @param places
	 */
	public CatsListOfListAdapter(Context contex, ArrayList<Place> places) {
		super();
		this.context = contex;
		this.places = places;
	}

	public CatsListOfListAdapter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
//		return places.size();
		
		return 2;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public View getView(int arg0, View convertView, ViewGroup parent) {
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.place_item, null);
        }	
		
//		ImageView img = (ImageView) convertView.findViewById(R.id.news_img);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.news_title);
		TextView txtdesc = (TextView) convertView.findViewById(R.id.news_desc);
		
		txtTitle.setText("Title " + arg0);
		txtdesc.setText("Description " + arg0);
		
		return convertView;
	}
}
