package com.Duleaf.Fujairah.Adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.Duleaf.Fujairah.Activities.R;
import com.Duleaf.Fujairah.Model.Artical;
import com.Duleaf.Fujairah.Model.FujConstants;

public class NewsListAdapter extends BaseAdapter {

	
	private Context context;
	private ArrayList<Artical> articals;
	
	
	
	
	/**
	 * @param context
	 * @param articals
	 */
	public NewsListAdapter(Context context, ArrayList<Artical> articals) {
		super();
		this.context = context;
		this.articals = articals;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		Log.i("Count : ",""+articals.size());
		return articals.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.news_item, null);
            
//            if(LangaugeController.getCurrentLangauge().equalsIgnoreCase(FujConstants.AR_Lang))
//            {
//                convertView = mInflater.inflate(R.layout.drawer_list_item_ar, null);
//            }
            
            convertView.setMinimumHeight(80);
            }


        TextView txtTitle = (TextView) convertView.findViewById(R.id.textView1);
        TextView txtdate = (TextView) convertView.findViewById(R.id.textView2);
        
        if(LangaugeController.getCurrentLangauge().equalsIgnoreCase(FujConstants.AR_Lang))
        {
        	txtTitle.setGravity(Gravity.RIGHT);
        	txtdate.setGravity(Gravity.RIGHT);
        }
        
         Artical artical = articals.get(position);
         
         txtTitle.setText(artical.getTitle());
         txtdate.setText(artical.getDate());

         
//        imgIcon.setImageResource(navDrawerItems.get(position).getIcon());        
//        txtTitle.setText(navDrawerItems.get(position).getTitle());

        return convertView;
	}

}
