package com.Duleaf.Fujairah.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

public class ShareData 
{

	
	public static void callNumber(String number,Context context)
	{

		 String uri = "tel:" + number.trim() ;
		 Intent intent = new Intent(Intent.ACTION_CALL);
		 intent.setData(Uri.parse(uri));
		 context.startActivity(intent);
	}
	
	public static void sendEmail(Context context ,String []recipient ,
			String subject , String body )
	{
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL  , recipient);
		i.putExtra(Intent.EXTRA_SUBJECT, subject);
		i.putExtra(Intent.EXTRA_TEXT   , body);
		try {
		    context.startActivity(Intent.createChooser(i, "Send mail..."));
		} catch (android.content.ActivityNotFoundException ex) {
		   Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
		}
	}

	
	public static void openUrl(String link,Context context)
	{
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(link));
		context.startActivity(i);
	}
	
}
