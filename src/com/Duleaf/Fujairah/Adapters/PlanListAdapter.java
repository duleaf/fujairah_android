package com.Duleaf.Fujairah.Adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.Duleaf.Fujairah.Activities.R;
import com.Duleaf.Fujairah.Model.Plan;

public class PlanListAdapter extends BaseAdapter {

	Context context;
	ArrayList<Plan> plans;
	
	
	/**
	 * @param context
	 * @param plans
	 */
	public PlanListAdapter(Context context, ArrayList<Plan> plans) {
		super();
		this.context = context;
		this.plans = plans;
	}

	@Override
	public int getCount() {
//		return plans.size();
		return 2;
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int arg0, View convertView, ViewGroup parent) {
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.plan_item, null);
        }	
		
//		ImageView img = (ImageView) convertView.findViewById(R.id.news_img);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.plan_title);
		TextView txtdesc = (TextView) convertView.findViewById(R.id.plan_date);
		
		txtTitle.setText("Title " + arg0);
		txtdesc.setText("19-Aug-2014,10:00 AM " + arg0);
		
		return convertView;
	}

}
