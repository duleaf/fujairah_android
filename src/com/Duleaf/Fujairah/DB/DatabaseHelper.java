package com.Duleaf.Fujairah.DB;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.Duleaf.Fujairah.Model.Artical;
import com.Duleaf.Fujairah.Model.Place;
import com.Duleaf.Fujairah.Model.PlaceCategory;
import com.Duleaf.Fujairah.Model.Plan;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 4;
 
    // Database Name
    private static final String DATABASE_NAME = "farook.db";
 
    
    // Table Names
    private static final String TABLE_PLACE  = "place";
    private static final String TABLE_PLAN   = "plan";
    private static final String TABLE_ARTICAL   = "artical";
    private static final String TABLE_GOVERNMENT_ENTITY   = "gov_entity";
    private static final String TABLE_PLACE_CAT   = "place_cat";
    

    // Common column names
    private static final String KEY_ID = "id";
    private static final String KEY_LANG = "lang";
    private static final String KEY_LAT = "lat";
    private static final String KEY_TITLE = "title";
    private static final String KEY_DESCRIPTION = "decription";
    private static final String KEY_ICON_URL = "icon_url";
    private static final String KEY_PHONE = "phone";
    private static final String KEY_COVER_IMG_URL = "cover_img_url";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_FB_URL = "fb_url";
    private static final String KEY_TW_URL = "TW_url";
    private static final String KEY_TYPE_ID = "type_id";
    
    
    //Plan
    private static final String KEY_DATE = "date";
    private static final String KEY_PlACE_ID = "place_id";

    

    // Table Create Statements
    // Catalog table create statement
    private static final String CREATE_TABLE_PLACE = "CREATE TABLE "
            + TABLE_PLACE + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TITLE
            + " TEXT," + KEY_DESCRIPTION + " TEXT," + KEY_COVER_IMG_URL + " TEXT," 
            + KEY_ICON_URL + " TEXT," + KEY_LANG + " TEXT," + KEY_LAT + " TEXT," 
            + KEY_PHONE + " TEXT," + KEY_TYPE_ID + " TEXT,"
            + KEY_EMAIL + " TEXT," + KEY_TW_URL + " TEXT,"  +  KEY_FB_URL 
            + " TEXT" + ")";
    
    
    private static final String CREATE_TABLE_PLAN = "CREATE TABLE "
            + TABLE_PLAN + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_DATE
            + " TEXT," + KEY_PlACE_ID + " INTEGER" + ")";
	
    private static final String CREATE_TABLE_ARTICAL = "CREATE TABLE "
            + TABLE_ARTICAL + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_DATE
            + " TEXT," + KEY_TITLE+ " TEXT," + KEY_COVER_IMG_URL+ " TEXT," + 
            KEY_DESCRIPTION+ " TEXT," + KEY_TYPE_ID+ " TEXT" +")";
    
    private static final String CREATE_TABLE_CATS = "CREATE TABLE "
            + TABLE_PLACE_CAT + "(" + KEY_ID + " INTEGER PRIMARY KEY," +
    		KEY_TITLE+ " TEXT" +")";
    
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
 
    @Override
    public void onCreate(SQLiteDatabase db) {
 
        // creating required tables
        db.execSQL(CREATE_TABLE_PLACE);
        db.execSQL(CREATE_TABLE_PLAN);
        db.execSQL(CREATE_TABLE_ARTICAL);
        db.execSQL(CREATE_TABLE_CATS);
        
        

        
    }
	
	public DatabaseHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLACE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTICAL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLACE_CAT);
        

        // create new tables
        onCreate(db);
    }
    
    
    //Place section
    
    public void createNewPlace(Place newPlace)
    {
	    SQLiteDatabase db = this.getWritableDatabase();
		 
	    ContentValues values = new ContentValues();
	    values.put(KEY_ID, newPlace.getId());
	    values.put(KEY_TITLE, newPlace.getTitle());
	    values.put(KEY_ICON_URL,newPlace.getIcon_url());
	    values.put(KEY_DESCRIPTION,newPlace.getDesc());
	    values.put(KEY_COVER_IMG_URL,newPlace.getCover_img_url());
	    values.put(KEY_LANG,newPlace.getLang());
	    values.put(KEY_LAT,newPlace.getLat());
	    values.put(KEY_PHONE,newPlace.getPhone());
	    values.put(KEY_EMAIL,newPlace.getEmail());
	    values.put(KEY_FB_URL,newPlace.getFb_url());
	    values.put(KEY_TW_URL,newPlace.getTw_url());
	    values.put(KEY_TYPE_ID,newPlace.getTypeId());
	    
	    
	    long catalog_id = db.insert(TABLE_PLACE, null, values);
	 

	    db.close();
    }
    
    
    public ArrayList<Place> selectPlaceFromType(String categoryId) {
		 
		ArrayList<Place> mainCategories = new ArrayList<Place>();
	    String selectQuery = "SELECT  * FROM " + TABLE_PLACE+ " WHERE "+KEY_TYPE_ID
	    		+" = \'"+categoryId+"\'";
	 
	 
	    SQLiteDatabase db = this.getReadableDatabase();
	    Cursor c = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    if (c.moveToFirst()) {
	        do {
	        	Place place = new Place();
	        	place.setId(c.getInt((c.getColumnIndex(KEY_ID))));
	        	place.setTitle((c.getString(c.getColumnIndex(KEY_TITLE))));
	        	place.setDesc(c.getString(c.getColumnIndex(KEY_DESCRIPTION)));
	        	place.setIcon_url(c.getString((c.getColumnIndex(KEY_ICON_URL))));
	            place.setCover_img_url(c.getString((c.getColumnIndex(KEY_COVER_IMG_URL))));
	            place.setLang(c.getString((c.getColumnIndex(KEY_LANG))));
	            place.setLat(c.getString((c.getColumnIndex(KEY_LAT))));
	            place.setTypeId(c.getString((c.getColumnIndex(KEY_TYPE_ID))));
	            place.setPhone(c.getString((c.getColumnIndex(KEY_PHONE))));
	            place.setFb_url(c.getString((c.getColumnIndex(KEY_FB_URL))));
	            place.setTw_url(c.getString((c.getColumnIndex(KEY_TW_URL))));
	            place.setEmail(c.getString((c.getColumnIndex(KEY_EMAIL))));
	            
	            // adding to Catalog list
	            mainCategories.add(place);
	        } while (c.moveToNext());
	    }
	    db.close();
	    return mainCategories;
	}

    
    public Place selectPlaceWithId(int categoryId) {
		 
	    String selectQuery = "SELECT  * FROM " + TABLE_PLACE+ " WHERE "+KEY_ID
	    		+" = \'"+categoryId+"\'";
	 
	 
	    SQLiteDatabase db = this.getReadableDatabase();
	    Cursor c = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    Place place = new Place();
	    if (c.moveToFirst()) {
	        
	        	
	        	place.setId(c.getInt((c.getColumnIndex(KEY_ID))));
	        	place.setTitle((c.getString(c.getColumnIndex(KEY_TITLE))));
	        	place.setDesc(c.getString(c.getColumnIndex(KEY_DESCRIPTION)));
	        	place.setIcon_url(c.getString((c.getColumnIndex(KEY_ICON_URL))));
	            place.setCover_img_url(c.getString((c.getColumnIndex(KEY_COVER_IMG_URL))));
	            place.setLang(c.getString((c.getColumnIndex(KEY_LANG))));
	            place.setLat(c.getString((c.getColumnIndex(KEY_LAT))));
	            place.setTypeId(c.getString((c.getColumnIndex(KEY_TYPE_ID))));
	            place.setPhone(c.getString((c.getColumnIndex(KEY_PHONE))));
	            place.setFb_url(c.getString((c.getColumnIndex(KEY_FB_URL))));
	            place.setTw_url(c.getString((c.getColumnIndex(KEY_TW_URL))));
	            place.setEmail(c.getString((c.getColumnIndex(KEY_EMAIL))));
	            
	            // adding to Catalog list
	       
	    }
	    db.close();
	    return place;
	}
    
    
    public void createNewPlane(Plan newPlan)
    {
	    SQLiteDatabase db = this.getWritableDatabase();
		 
	    ContentValues values = new ContentValues();
	    values.put(KEY_ID, newPlan.getPlanId());
	    values.put(KEY_PlACE_ID, newPlan.getPlace().getId());
	    values.put(KEY_DATE,newPlan.getDate());
	    
	    
	    long catalog_id = db.insert(TABLE_PLACE, null, values);
	 

	    db.close();
    }
    
    public ArrayList<Plan> getAllPlans() {
		 
		ArrayList<Plan> plans = new ArrayList<Plan>();
	    String selectQuery = "SELECT  * FROM " + TABLE_PLACE;
	 
	 
	    SQLiteDatabase db = this.getReadableDatabase();
	    Cursor c = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    if (c.moveToFirst()) {
	        do {
	        	Plan plan = new Plan();
	        	plan.place = new Place();
	        	
	        	plan.setPlanId(c.getInt((c.getColumnIndex(KEY_ID))));
	        	plan.setDate((c.getString(c.getColumnIndex(KEY_DATE))));
	        	int placeId = (c.getInt(c.getColumnIndex(KEY_PlACE_ID)));

	        	plan.place = selectPlaceWithId(placeId);
	            
	            // adding to Catalog list
	            plans.add(plan);
	        } while (c.moveToNext());
	    }
	    db.close();
	    return plans;
	}
    
    
    public void createNewArtical(Artical artical)
    {
	    SQLiteDatabase db = this.getWritableDatabase();
		 
	    ContentValues values = new ContentValues();
	    values.put(KEY_ID, artical.getId());
	    values.put(KEY_TITLE, artical.getTitle());
	    values.put(KEY_DESCRIPTION,artical.getDetails());
	    values.put(KEY_COVER_IMG_URL,artical.getImage());
	    values.put(KEY_TYPE_ID,artical.getType());
	    values.put(KEY_DATE,artical.getDate());
	    
	    
	    
	    long catalog_id = db.insert(TABLE_ARTICAL, null, values);
	 

	    db.close();
    }
    
    
    public ArrayList<Artical> getAllArticals() {
		 
		ArrayList<Artical> articals = new ArrayList<Artical>();
	    String selectQuery = "SELECT  * FROM " + TABLE_ARTICAL;
	 
	 
	    SQLiteDatabase db = this.getReadableDatabase();
	    Cursor c = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    if (c.moveToFirst()) {
	        do {
	        	Artical artical = new Artical(c.getString(c.getColumnIndex(KEY_TITLE))
	        			, c.getString(c.getColumnIndex(KEY_DESCRIPTION))
	        			, c.getString(c.getColumnIndex(KEY_COVER_IMG_URL))
	        			, c.getString(c.getColumnIndex(KEY_TYPE_ID)),
	        			c.getString(c.getColumnIndex(KEY_DATE)),
	        			c.getInt((c.getColumnIndex(KEY_ID))));
	        	
	        	
	        	articals.add(artical);
	        } while (c.moveToNext());
	    }
	    db.close();
	    return articals;
	}
    
    
    public void deleteALLArticals() {
		
		List<Artical> articals = this.getAllArticals();
	    SQLiteDatabase db = this.getWritableDatabase();
	    
	    for (Artical art : articals) {
		    db.delete(TABLE_ARTICAL, KEY_ID + " = ?",
		            new String[] { String.valueOf(art.getId()) });
		}
	    db.close();
	}
    
    public void createNewCategory(PlaceCategory cat)
    {
	    SQLiteDatabase db = this.getWritableDatabase();
		 
	    ContentValues values = new ContentValues();
	    values.put(KEY_ID, cat.getId());
	    values.put(KEY_TITLE, cat.getName());
	    
	    Log.i("Insert Object", cat.toString());
	    long catalog_id = db.insert(TABLE_PLACE_CAT, null, values);
	 

	    db.close();
    }
    
    
    public ArrayList<PlaceCategory> getAllCategories() {
		 
		ArrayList<PlaceCategory> cats = new ArrayList<PlaceCategory>();
	    String selectQuery = "SELECT  * FROM " + TABLE_PLACE_CAT;
	 
	 
	    SQLiteDatabase db = this.getReadableDatabase();
	    Cursor c = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    if (c.moveToFirst()) {
	        do {
	        	
	        	PlaceCategory cat = new PlaceCategory(c.getString(c.getColumnIndex(KEY_TITLE)),
	        			c.getInt(c.getColumnIndex(KEY_ID)));
	        	
	        	Log.i("Cat", cat.toString());
	        	
	        	cats.add(cat);
	        } while (c.moveToNext());
	    }
	    db.close();
	    return cats;
	}
    
    
    public void deleteALLCategories() {
		
		List<Artical> articals = this.getAllArticals();
	    SQLiteDatabase db = this.getWritableDatabase();
	    
	    for (Artical art : articals) {
		    db.delete(TABLE_PLACE_CAT, KEY_ID + " = ?",
		            new String[] { String.valueOf(art.getId()) });
		}
	    db.close();
	}
}
